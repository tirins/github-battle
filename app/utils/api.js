const id     = 'GITHUB_CLIENT_ID' //only need this is in case GH rate limits us
const skey   = 'GITHUB_SECRET_KEY'
const params = `?client_id=${id}&client_secret=${skey}`

function getErrorMsg ( msg, username ) {
  if ( msg === 'Not Found' ) {
    return `${username} does not exist`
  }

  return msg
}

function getProfile ( username ) {
  return fetch( `https://api.github.com/users/${username}${params}` )
    .then( ( res ) => res.json() )
    .then( ( profile ) => {
      if ( profile.message ) {
        throw new Error( getErrorMsg( profile.message, username ) )
      }

      return profile

    } )
}

function getRepos ( username ) {
  return fetch( `https://api.github.com/users/${username}/repos${params}&per_page=100` )
    .then( ( res ) => res.json() )
    .then( ( repos ) => {
      if ( repos.message ) {
        throw new Error( getErrorMsg( repos.message, username ) )
      }

      return repos
    } )
}

function getStarCount ( repos ) {
  return repos.reduce( ( count, { stargazers_count: stars } ) => count + stars, 0 )
}

function calculateScore ( followers, repos ) {
  return followers * 3 + getStarCount( repos )
}

function getUserData ( player ) {
  return Promise.all( [
    getProfile( player ),
    getRepos( player ),
  ] ).then( ( [ profile, repos ] ) => ({
    profile,
    score: calculateScore( profile.followers, repos ),
  }) )
}

function sortPlayers ( players ) {
  return players.sort( ( a, b ) => b.score - a.score )
}

export function battle ( players ) {
  return Promise.all( [
    getUserData( players[ 0 ] ),
    getUserData( players[ 1 ] ),
  ] )
    .then( ( results ) => {
      return sortPlayers( results )
    } )
}

export function fetchPopularRepos ( lang ) {
  const endpoint = window.encodeURI( `https://api.github.com/search/repositories?q=stars:>1+language:${lang}&sort=stars&order=desc&type=Repositories` )

  return fetch( endpoint )
    .then( res => res.json() )
    .then( data => {
      if ( !data.items ) {
        throw new Error( data.message )
      }

      return data.items
    } )
}
