import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import PropTypes from 'prop-types'
import { ThemeProvider } from './contexts/theme'
import MainNav from './components/MainNav'
import {
  Route,
  Link,
  BrowserRouter as Router,
  Redirect,
  Switch,
} from 'react-router-dom'
import Loading from './components/Loading'

const Popular = React.lazy( () => import('./components/Popular') )
const Battle  = React.lazy( () => import('./components/Battle') )
const Results = React.lazy( () => import('./components/Results') )


class App extends React.Component {
  state = {
    'current': 'Popular',
    'routes': [ 'Popular', 'Battle' ],
    'theme': 'light',
    toggleTheme: () => {
      this.setState( ( { theme } ) => ({
        theme: theme === 'light' ? 'dark' : 'light',
      }) )
    },
  }

  updateRoute = current => {
    this.setState( { current } )
  }

  render () {
    const { current, routes } = this.state
    return (
      <Router>
        <ThemeProvider value={this.state}>

          <div className={this.state.theme}>
            <MainNav routes={routes} current={current} onUpdateSelectedRoute={( route ) => this.updateRoute( route )}/>
            <div className="container">
              {/*{current === 'Battle' && (<Battle/>)}
              {current === 'Popular' && (<Popular/>)}*/}
              <React.Suspense fallback={<Loading/>}>
                <Switch>
                  <Route exact path={'/'} component={Popular}/>
                  <Route exact path={'/battle'} component={Battle}/>
                  <Route path={'/battle/results'} component={Results}/>
                  <Route exact path={'/Popular'} render={() => (<Redirect to={'/'}/>)}/>
                  <Route render={() => <h1>404</h1>}/>
                </Switch>
              </React.Suspense>
            </div>
          </div>

        </ThemeProvider>
      </Router>
    )
  }
}

ReactDOM.render(
  //React Element to render
  (<App/>),

  //Dom Node to render the element to
  document.getElementById( 'app' ),
)
