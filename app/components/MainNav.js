import PropTypes from 'prop-types'
import React from 'react'
import { ThemeConsumer } from '../contexts/theme'
import { NavLink, BrowserRouter as Router } from 'react-router-dom'

const activeStyle = {
  color: 'rgb(187, 46, 31)'
}
function MainNav (  ) {
  return (
    <React.Fragment>
      <div className="row space-between">
        <ul className="row nav">
          <li >
            <NavLink
              exact
              to={`/`}
              className={'btn-clear nav-link'}
              activeStyle={activeStyle}
            >
             Popular
            </NavLink>
          </li>
          <li >
            <NavLink
              to={`/battle`}
              className={'btn-clear nav-link'}
              activeStyle={activeStyle}
            >
              Battle
            </NavLink>
          </li>
        </ul>
        <ThemeConsumer>
          {( { theme, toggleTheme } ) => (
            <button
              style={{ fontSize: 30 }}
              onClick={toggleTheme}>
              {theme === 'light' ? '🔆' : '😎'}
            </button>
          )}
        </ThemeConsumer>
      </div>

    </React.Fragment>
  )
}

export default MainNav
