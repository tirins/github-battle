import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { battle } from '../utils/api'
import { FaCompass, FaBriefcase, FaUsers, FaCode, FaUser, FaUserFriends } from 'react-icons/fa'
import Card from './Card'
import Loading from './Loading'
import Tooltip from './Tooltip'
import queryString from 'query-string'
import { Link, Redirect } from 'react-router-dom'


function ProfileList ( { profile } ) {
  return (
    <ul className={'card-list'}>
      <li>
        <Tooltip text={'Profile Name'}>
          <FaUser color={'rgb(239, 115,115)'} size={22}/>{profile.name}
        </Tooltip>
      </li>
      {profile.location && (
        <li>
          <Tooltip text={'Users Location'}>
            <FaCompass color={'rgb(144, 116,255)'} size={22}/>{profile.location}
          </Tooltip>
        </li>)
      }
      {profile.company && (
        <li><FaBriefcase color={'#795548'} size={22}/>{profile.company}</li>)
      }
      <li><FaUsers color={'rgb(129, 195,245)'} size={22}/>{profile.followers.toLocaleString()} followers</li>
      <li><FaUserFriends color={'rgb(64, 195,183)'} size={22}/>{profile.following.toLocaleString()} following</li>
      <li><FaCode color={'rgb(64, 195,0)'} size={22}/>{profile.public_repos.toLocaleString()} repos</li>
    </ul>
  )
}

ProfileList.propTypes = {
  profile: PropTypes.object.isRequired,
}

export default class Results extends Component {
  state = {
    winner: null,
    loser: null,
    loading: true,
    error: null,
  }

  componentDidMount () {
    const data = queryString.parse( this.props.location.search )
    const { playerOne, playerTwo } = queryString.parse( this.props.location.search )

    if ( !playerOne || !playerTwo ) {
      return  this.props.history.replace(`/battle`)
    }

    battle( [ playerOne, playerTwo ] )
      .then( ( players ) => this.setState( {
        winner: players[ 0 ],
        loser: players[ 1 ],
        error: null,
        loading: false,
      } ) )
      .catch( ( { message } ) => {
        this.setState( {
          error: message,
          loading: false,
        } )
      } )
  }

  render () {
    const { winner, loser, error, loading } = this.state

    if ( loading ) {
      return <Loading text={'Battling'} speed={100}/>
    }

    if ( error ) {
      return (
        <p className={'center-text error'}>
          {error}
        </p>
      )
    }

    return (
      <React.Fragment>
        <div className={'grid space-around container-sm'}>
          <Card
            header={winner.score === loser.scope ? 'Tie' : 'Winner'}
            subheader={`Score: ${winner.score.toLocaleString()}`}
            avatar={winner.profile.avatar_url}
            name={winner.profile.login}
            href={winner.profile.html_url}
          >
            <ProfileList profile={winner.profile}/>
          </Card>

          <Card
            header={winner.score === loser.scope ? 'Tie' : 'Loser'}
            subheader={`Score: ${loser.score.toLocaleString()}`}
            avatar={loser.profile.avatar_url}
            name={loser.profile.login}
            href={loser.profile.html_url}
          >
            <ProfileList profile={loser.profile}/>
          </Card>

          {/*<h1>Results</h1>
        <pre>{JSON.stringify( this.state, null, 2 )}</pre>*/}
        </div>
        <Link
          className={'btn dark-btn btn-space'}
          to={'/battle'}>
          Reset
        </Link>
      </React.Fragment>
    )
  }
}

